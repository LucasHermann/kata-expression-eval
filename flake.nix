{
  description = "Expression Eval Kata from CodeWorks";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... } :
  flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {inherit system; };

      # Specify which version of Python to use
      py-ver = pkgs.python39;

      # List all the Python dependencies that are packaged in nixpkgs
      python-deps = py: with py; [
        black
      ];

      python-env = py-ver.withPackages python-deps;
    in {
      devShells.default = pkgs.mkShell {
        buildInputs = [
          python-env
        ];
      };
    }
  );
}
